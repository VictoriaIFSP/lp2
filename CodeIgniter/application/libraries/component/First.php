<?php

class First extends TestCase {
    public function setUp(): void{
        $this->resetInstance();
    }

    function tesHello(){
        $this->CI->load->library('conta');
        $res = $this->CI->conta->soma(2, 3);
        $this->assertEquals(5, $res);
    }

    function testContaDeveRetornarRegistrosDoMesDezembro(){
        $this->CI->load->library('conta');
        $res = $this->CI->conta->lista('pagar', 12, 2020);
        $this->assertEquals(4, sizeof($res));
    }

    function testContaDeveInserirRegistroCorretamente() {
        $conta = ['parceiro' => 'Magalu', 'descricao' => 'Notebook', 'valor' => '2000', 'mes' => '01', 'ano' => '2021'];

        $this->CI->load->library('conta');
        $res = $this->CI->conta->cria($conta);
        $res = $this->CI->conta->lista('pagar', 12, 2020);

    }
}