<?php

defined('BASEPATH') OR exit('No direct script acess allowed');
include_once APPPATH.'libraries/util/CI_Object.php';

/**
 * 
 * 
 *  @param array: data - os dados da conta
*/
    class Conta extends CI_Object {
        function cria($data){
            $this->db->insert('conta', $data);
            return $this->db->insert_id();
        }

        /**
         * Gera uma lista de contas
         *
         * @param string tipo: o tipo da cnta
         * @param int mes: mes de acerto da conta
         * @param int ano: ano de acerto de conta
         */
        public function lista($tipo, $mes = 0, $ano = 0){
                $data = ['tipo' => $tipo, 'mes' => $mes, 'ano' => $ano];
                $res = $this->db->get_where('conta', $data);
                return $res->result_array();
        }
        public function delete($data){
            $this->db->delete('conta', $data);
        }

        public function edita($data){
            $this->db->update('conta', $data);
        }
    }