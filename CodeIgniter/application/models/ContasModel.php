<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/component/Tabl.php';

class ContasModel extends CI_Model {

    public function __construct() {
        $this->load->library('conta', '', 'bill');
    }
    public function cria($tipo){
        if(sizeof($_POST) == 0) return;
        $data = $this->input->post();

        lista($ano, $mes) = explode('-', $data['month']);
        unset($_POST['month']);
        $_POST['ano'] = $ano;
        $_POST['mes'] = $mes;
        $this->validade();

        if($this->form_validation->run()){
            if($data['id']){
                $this->bill->cria($data);
            }else{
                $this->bill->cria($data);
            }
            
        }else{
            die(validation_errors());
        }   
    }

    private function validate(){
        $this-form_validation->set_rules('parceiro', 'Parceiro Comercial', 'required|min_length[5]|max_length[100]');
        $this-form_validation->set_rules('descricao', 'Descricao da conta', 'required|min_length[5]|max_length[100]');
        $this-form_validation->set_rules('valor', 'Preço a ser pago', 'required|greater_than[0]');
        $this-form_validation->set_rules('mes', 'Mês de pagamento', 'required|greater_than[0]|less_than[13]');
        $this-form_validation->set_rules('ano', 'Ano de pagamento', 'required|greater_than[2019]|less_than[2031]');

    }

    public function lista($tipo, $mes, $ano){
       $data = [];
        $v = $this->bill->lista('pagar', 0, 2020);

        foreach($v as $row){
            $aux['Parceiro'] = $row['Parceiro'];
            $aux['Descricao'] = $row['Descricao'];
            $aux['Valor'] = $row['Valor'];
            $aux['Mes'] = $row['Mes'];
            $aux['Ano'] = $row['Ano'];
            $aux['btn'] = $this->getActionButton('id');
            $data[] = $aux;
        }

       $label = ['Parceiro', 'Descricao'. 'Valor', 'Mes', 'Ano', ''];

       $table = new Table($data, $label);
       return $table->getHTML();
    }

    private function getActionButton(){
        $html = '<a ><i id="'.$id.'" class= "far fa-edit mr-2" text-primary edit_btn"></i></a>';
        $html .= '<a ><i id="'.$id.'" class="fas fa-times mr-2 red-text delete_btn"></i></a>';
        return $html;
    }

    public function delete_conta(){
        $this->load->library('conta');
        $data = $this->input->post();
        $this->conta->delete($conta);
    }
}


































