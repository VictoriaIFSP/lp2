<div class="container">


<a
  class="btn btn-primary"
  data-mdb-toggle="collapse"
  href="#collapseExample"
  role="button"
  aria-expanded="false"
  aria-controls="collapseExample"
>
  Link with href
</a>
<button
  class="btn btn-primary"
  type="button"
  data-mdb-toggle="collapse"
  data-mdb-target="#collapseExample"
  aria-expanded="false"
  aria-controls="collapseExample"
>
  Button with data-mdb-target
</button>


<div class="collapse mt-3" id="collapseExample">
<div class="row">
        <div class="col-md-6-mx-auto border-5 pt-5">
            <form method="POST" id="contas-form">

                <input class="form-control" value="<?= set_value('parceiro') ?>" id="parceiro" name="parceiro" type="text" placeholder="Devedor/Credor"><br>
                <input class="form-control"  value="<?= set_value('descricao') ?>" id="descricao" name="descricao"  type="text" placeholder="Descrição"><br>
                <input class="form-control"  value="<?= set_value('valor') ?>"id="valor" name="valor" type="number" placeholder="Valor"><br><br>
                
                <input type="hidden" name="id" id="conta_id">
                <input type="hidden" name="tipo" value="<?= $tipo ?>">

                <?php echo form_error('parceiro', '<div class=="error">', '</div>'); ?>
                <?php echo form_error('descricao', '<div class=="error">', '</div>'); ?>
                <?php echo form_error('ano', '<div class=="error">', '</div>'); ?>
                <?php echo form_error('valor', '<div class=="error">', '</div>'); ?>
                <?php echo form_error('mes', '<div class=="error">', '</div>'); ?>

                <div class="text-center text-md-left">
                
                   <a class="btn btn-primary" onclick="document.getElementById('contas-form').submit();" ></a>
                </div>

            </form>
        </div>
    </div>
</div>
   

    <div class="row mt-5">
    <div class="col">
        <?= $lista ?>
    </div>

    </div>
</div>

<script>
$(document).ready(function){
  $('#month').change(load);
  $('.delete_btn').click(openModal);
  $('#confirmBtn').click(deleteRow);
  $('.edit_btn').click(exibeForm);
  $('.pay_btn').click(exibeForm);
}};

function loadMonth(){
   var data = this.value;
  var ano = data[0];
  var mes = data[1];

  var v = window.location.href.split('/');
  var url = v.slice(0, 6).join('/');
  url = url + '/' + mes + '/' + ano;
  window.location.href = url;
}

function exibeForm(){
  var row_id = this.id;
  var td = $('#' + row_id).parent().parent().parent().children();
  
  $('#parceiro').val$(td[0].text());
  $('#descricao').val$(td[1].text());
  $('#valor').val$(td[2].text());
  $('#mesr').val$(td[3].text());
  $('#parceiro').val$(td[4].text());
  $('#conta_id').val(row_id);

  $('#collapseForm').collapse('show');
}

function openModal(){
  row_id = this.id;
  $('#basicExampleModal').modal();
}

function deleteRow(){
  var id = this.id;
  $.post(api('conta', 'delete_conta'),(id), function(d, s, x) (console.log(x.responseText)));
  $('#' + row_id).parent().parent().parent().remove()
  $('#basicExampleModal').modal('hide');
}

</script>